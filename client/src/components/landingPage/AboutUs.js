import React from "react";

function AboutUs(){
    const image="https://i.ytimg.com/vi/4TOy_8_h4n4/maxresdefault.jpg"
    const image1="https://www.callofduty.com/content/dam/atvi/callofduty/cod-touchui/mw2/home/reveal/new-era/new_era-mw2.jpg";
    return(
        <div className="container-fluid bg-dark">
            <div class="rounded container" style={{backgroundColor:"rgba(217,217,217,0.1)"}}>
                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-8 ">
                    <div class="col" style={{paddingLeft:"8%"}}>
                        <p className="h3 text-white mt-4 text-start">About Us</p>
                        <p className="h5 text-white mt-4 text-start"> Assets are the various elements that make up a game, including graphics, sounds, music, animations, and 3D models. Asset building involves creating or sourcing these assets to be used in your game. You can either create assets from scratch using software tools like graphic design software or 3D modeling programs, or you can use pre-made assets from online marketplaces or open-source libraries.<br/>Here</p>
                    
                    </div>
                    <div class="col">
                        <img src={image} class="container-fluid img-fluid rounded float-left"/>
                    </div>
                    
                </div>
            </div>
            <hr className="mt-5 container " style={{color:'white'}}/>
            <div className="container">
                <h3 className="mt-5 text-white text-start container-fluid">Our Story</h3>
                {/* <p className="text-white text-start container-fluid">
                    Long ago and far away, in Estonia in the early 2000s, 
                    our co-founders ran a software dlkjaslkdjlkasdl jdslkjasldj  
                    sdljalsdjfl  jkasjflkasjdlfkjasldkfjla   consultancy.
                </p> */}
                <div class=" mt-5 row row-cols-1 row-cols-sm-2 row-cols-md-8 ">
                    <div class="col">
                        <img src={image1} class="container-fluid img-fluid rounded float-left"/>
                    </div>
                    <div class="col ps-3">

                        <p className="h2 text-white mt-4 text-start">Vision</p>
                        <p className="h6 text-white mt-4 text-start">Our vision is to become the leading web application for asset management, serving as a trusted and indispensable tool for individuals, creative professionals, and businesses of all sizes. We envision a future where users can effortlessly manage their digital assets, whether it be images, videos, documents, or other media, with robust features for categorization, metadata management, version control, and collaboration. We aim to continuously innovate and adapt to emerging technologies, ensuring our users have a seamless and secure experience in managing their valuable digital resources.</p>

                        <p className="h2 text-white mt-4 text-start">Mission</p>
                        <p className="h6 text-white mt-4 text-start">Our mission at Assets Vault Web App is to provide a secure and efficient platform for managing and organizing digital assets. We aim to empower individuals and businesses to easily store, search, and access their valuable assets in a centralized and user-friendly environment. By offering a reliable solution, we strive to streamline asset management processes, enhance productivity, and protect the integrity and confidentiality of our users' digital content.</p>
                    </div>
                </div>
                <h2 className="text-white  text-start container-fluid mt-5">
                    The Team
                </h2>
                <div class="container-fluid mt-3 text-center p-4"style={{backgroundColor:"rgba(217,217,217,0.1)"}}>
                    <div class="row row-cols-1 row-cols-sm-2 row-cols-md-4">
                        <div class="col">
                            <div class="card" style={{width: "18rem;"}}>
                                <img src={image} class="card-img-top" alt="..."/>
                                <div class="card-body">
                                    <p class="card-text h5">Dorji Lethro</p>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                        <div class="card" style={{width: "18rem;"}}>
                                <img src={image1} class="card-img-top" alt="..."/>
                                <div class="card-body">
                                    <p class="card-text h5">Sherab Doji</p>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                        <div class="card" style={{width: "18rem;"}}>
                                <img src={image1} class="card-img-top" alt="..."/>
                                <div class="card-body">
                                    <p class="card-text h5">Rinchen Pelzang</p>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                        <div class="card" style={{width: "18rem;"}}>
                                <img src={image1} class="card-img-top" alt="..."/>
                                <div class="card-body">
                                    <p class="card-text h5">Gyelden Dorji</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AboutUs;