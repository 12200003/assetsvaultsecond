import { Link, NavLink, Outlet, useNavigate } from "react-router-dom";
import LoginModal from "../user/LoginModal";
import { useEffect, useState } from "react";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {Sentry} from 'react-activity';
import 'react-activity/dist/library.css';

export default function RootLayouts({setAuth}) {

    const navigate = useNavigate();

    const [isOpen,setIsOpen] = useState(false);
    const [isSigningUp,setIsSigningUp] = useState(false);

    //register
    const [signUpEmail,setSignUpEmail] = useState("");
    const [userName,setUserName] = useState("");
    const [password,setPassword] = useState("");
    const [confirmPassword,setConfirmPassword] = useState("");

    //login
    const [loginEmail,setLoginEmail] = useState();
    const [loginPassword,setLoginPassword] = useState();

    const [userLoggingIn,setUserLogingIn] = useState(false);
    const [userVerification,setUserVerification] = useState(false);


    const gettingOTP = async (e) => {
        e.preventDefault();
        setUserVerification(true);
        try {
            const body = {email:signUpEmail,password,name:userName,confirm:confirmPassword};
            const response = await fetch("http://localhost:5000/sendOTP",{
                method:'POST',
                headers: {"Content-Type":"application/json"},
                body:JSON.stringify(body)
            })

            const parseRes = await response.json();
            console.log(parseRes);
            if (parseRes.success === true) {
                setUserVerification(false)
                navigate('/otpverification', {state:{otp:parseRes.otp,email:signUpEmail,password,name:userName,confirm:confirmPassword}})
            } else {
                toast.error(parseRes, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "colored",
                });
                setUserVerification(false)
            }
        } catch (error) {
            toast.error(error.message, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "colored",
            });
            setUserVerification(false)
        }
    }

    const onSignUp = async (e) => {
        e.preventDefault();
        try {
            const body = {email:signUpEmail,password,name:userName,confirm:confirmPassword};
            const response = await fetch("http://localhost:5000/auth/register",{
                method:'POST',
                headers:{"Content-Type":"application/json"},
                body:JSON.stringify(body)
            });
            const parseRes = await response.json();

            if (!parseRes.token) {
                toast.error(parseRes, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "colored",
                });
            } else if (parseRes.token) {
                toast.success('🦄 Registered Successfully!', {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "colored",
                    });
                setSignUpEmail("");
                setUserName("");
                setPassword("");
                setConfirmPassword("");
                setIsSigningUp(false)
            }
        } catch (error) {
            console.error(error.message);
        }
    }

    const onLogin = async (e) => {
        e.preventDefault();
        setUserLogingIn(true)
        try {
            const body = {email:loginEmail,password:loginPassword}
            const response = await fetch("http://localhost:5000/auth/login",{
                method:'POST',
                headers:{"Content-Type":"application/json"},
                body:JSON.stringify(body)
            })
            const parseRes = await response.json();

            if (parseRes.token) {
                localStorage.setItem("token",parseRes.token);
                localStorage.setItem("userId",parseRes.userId)
                setAuth(true)
                setUserLogingIn(false);
                
            } else {
                setAuth(false);
                toast.error(parseRes, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "colored",
                    });
                setUserLogingIn(false);
            }
            console.log(parseRes);
        } catch (error) {
            console.error(error.message);
            setUserLogingIn(false);
        }
    }

    console.log(signUpEmail)
    console.log(password)
    console.log(userName)
    console.log(confirmPassword)

    useEffect(() => {
        const token = localStorage.getItem("token")
        const userId = localStorage.getItem("userId")

        if (token && userId) {
            setAuth(true)
        }
    },[])

    return(
        <div className = "container-fluid bg-dark h-100">
            <header className="">
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                    <div className="container-fluid mt-5 ms-5 me-5">
                        <NavLink to = "/" className="text-decoration-none text-white" style={{fontFamily:'customFont'}}><h1 className="d-none d-md-flex text-white" style={{fontFamily:'customFont', fontSize:51}}><span style={{color:"#FE4C00"}}>Assets</span>Vault</h1></NavLink>
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className="nav-item ms-5">
                                <NavLink to = "aboutus" className="nav-link text-white" style={{fontFamily:'customFont'}}>About Us</NavLink>
                            </li>
                        </ul>
                        
                        <button className="btn text-white pe-4 ps-4" style={{backgroundColor:"#FE4C00"}} type="submit" onClick={() => setIsOpen(true)}>Sign In</button>
                        <LoginModal open = {isOpen} onClose={() => setIsOpen(false)}>
                            <div>
                                <div className="d-flex justify-content-evenly mb-4">
                                    <div className="" style={{paddingRight:30, paddingLeft:26, cursor:'pointer',backgroundColor:!isSigningUp?'#FE4C00':null,color:!isSigningUp?'white':'black'}} onClick={() => setIsSigningUp(false)}>
                                        <h3>Log In</h3>
                                    </div>
                                    <div className="" style={{paddingRight:30, paddingLeft:26,backgroundColor:isSigningUp?'#FE4C00':null, cursor:'pointer', color:isSigningUp?'white':'black'}} onClick={() => setIsSigningUp(true)}>
                                        <h3>Sign Up</h3>
                                    </div>
                                </div>
                                
                                {isSigningUp && 
                                    <form className="d-flex flex-column row g-3" onSubmit={gettingOTP}>
                                        <div class="col">
                                            <label for="validationServer05" className="form-label">Email</label>
                                            <input type="text" className="form-control"
                                                value={signUpEmail}
                                                onChange = {e => setSignUpEmail(e.target.value)}
                                            />
                                            <div id="validationServer05Feedback" className="invalid-feedback">
                                            Please provide a valid email.
                                            </div>
                                        </div>
        
                                        <div class="col">
                                            <label for="validationServer05" className="form-label">Full Name</label>
                                            <input type="text" className="form-control"
                                                value={userName}
                                                onChange={e => setUserName(e.target.value)}
                                            />
                                        </div>
        
                                        <div class="col">
                                            <label for="validationServer05" className="form-label">Password</label>
                                            <input type="password" className="form-control"
                                                value={password}
                                                onChange={e => setPassword(e.target.value)}
                                            />
                                            <div id="" className="" style={{fontSize:13,color:'green'}}>
                                            Make your password mix of letters,digits and symbol
                                            </div>
                                        </div>
        
                                        <div class="col">
                                            <label for="validationServer05" className="form-label">Confirm Password</label>
                                            <input type="password" className="form-control"
                                                value={confirmPassword}
                                                onChange={e => setConfirmPassword(e.target.value)}
                                            />
                                        </div>
        
                                        <button type="submit" className="btn" style={{backgroundColor:'#FE4C00', color:'white'}}>{userVerification?<Sentry/>:"Register"}</button>
    
                                    </form>
                                }
                                {!isSigningUp && 
                                    <div>
                                        <h4 className="text-center" style={{fontWeight:'bold'}}>Welcome 👏👏👏</h4>
                                        <form className="d-flex flex-column row g-3" onSubmit={onLogin}>
                                            <div class="col">
                                                <label for="validationServer05" className="form-label">Email</label>
                                                <input type="text" className="form-control"
                                                    value={loginEmail}
                                                    onChange={e => setLoginEmail(e.target.value)}
                                                />
                                            </div>
            
                                            <div className="col">
                                                <label for="validationServer05" className="form-label">Password</label>
                                                <input type="password" className="form-control"
                                                    value={loginPassword}
                                                    onChange={e => setLoginPassword(e.target.value)}
                                                />
                                                <Link className="text-decoration-none" to = "/forgotpassword"><p style={{color:"blue", fontSize:13, textAlign:'right', cursor:'pointer'}} onClick={() => console.log("hello")}>Forgot your password?</p></Link>
                                            </div>
            
                                            <button className="btn" style={{backgroundColor:'#FE4C00', color:'white'}}>{userLoggingIn?<Sentry/>:"Sign In"}</button>
        
                                        </form>
                                    </div>     
                                }
                            </div>
                        </LoginModal>
                        </div>
                    </div>
                </nav>
            </header>
            <main className="container-fluid">
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
            />

            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
            />
                <Outlet/>
            </main>
        </div>
    )
}