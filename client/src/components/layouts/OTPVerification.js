import React, { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate } from 'react-router-dom';
import {Sentry} from 'react-activity';
import 'react-activity/dist/library.css';

export default function OTPVerification() {
    const navigate = useNavigate();
    const location = useLocation();

    const [OTP,setOTP] = useState("");
    const [enteredOTP,setEnteredOTP] = useState("");
    const [resendingOTP,setResendingOTP] = useState(false);
    const [registering,setRegistering] = useState(false);

    const [countDown,setCountDown] = useState(20);

    const {otp,email, name, password,confirm} = location.state;

    useEffect(() => {
        setOTP(otp);
    },[])

    const gettingOTP = async () => {
        setCountDown(20)
        setResendingOTP(true);
        try {
            const body = {email:email,password,name:name,confirm:confirm};
            const response = await fetch("http://localhost:5000/sendOTP",{
                method:'POST',
                headers: {"Content-Type":"application/json"},
                body:JSON.stringify(body)
            })

            const parseRes = await response.json();
            console.log(parseRes);
            if (parseRes.success === true) {
                setOTP(parseRes.otp);
                setResendingOTP(false)
            } else {
                toast.error(parseRes, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "colored",
                });
                setResendingOTP(false)
            }
        } catch (error) {
            console.log(error.message);
            setResendingOTP(false)
        }
    }

    useEffect(() => {
        if (countDown <= 0) {
            // Countdown has reached zero
            // Perform any action you want here
            // For example, display a message or execute a function
            console.log("Countdown reached zero!");
          } else {
            // Countdown is still running
            const timer = setTimeout(() => {
              setCountDown(countDown - 1);
            }, 1000);
      
            // Clean up the timer when the component unmounts or the countdown changes
            return () => clearTimeout(timer);
          }
    },[countDown, OTP])

    const onSignUp = async () => {
        try {
            const body = {email:email,password,name:name,confirm:confirm};
            const response = await fetch("http://localhost:5000/auth/register",{
                method:'POST',
                headers:{"Content-Type":"application/json"},
                body:JSON.stringify(body)
            });
            const parseRes = await response.json();

            if (!parseRes.token) {
                toast.error(parseRes, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "colored",
                });
                setRegistering(false)
            } else if (parseRes.token) {
                toast.success('🦄 Registered Successfully!', {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "colored",
                });
                
                setTimeout(() => {
                    navigate("/")
                },5000)


                // setSignUpEmail("");
                // setUserName("");
                // setPassword("");
                // setConfirmPassword("");
                // setIsSigningUp(false)
            }
        } catch (error) {
            console.error(error.message);
            setRegistering(false)
        }
    }

    const registerNow = async () => {
        setRegistering(true)
        if (enteredOTP === OTP) {
            onSignUp();
        } else {
            toast.error("OTP mismatch!", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "colored",
            });
            setRegistering(false);
        }
    }

    // useEffect(() => {
    //     const redirectTimer = setTimeout(() => {
    //         navigate('/');
    //       }, 5000);
      
    //       return () => {
    //         clearTimeout(redirectTimer);
    //       };
    // },[goBack])

    console.log("otp ",OTP);
    console.log("enterd otp",enteredOTP)
  return (
    <div className='text-center d-flex justify-content-center align-items-center'>

        <div style={{width:300}}>
            <h5 className='text-white mb-5'>Please Check Your Mail.</h5>

            {countDown && countDown !== 0 ? (
                <div>
                    <h6 className='text-white'>If you haven't received the OTP, you can resend in {countDown} second</h6>
                </div>
            ):(
                <div>
                    <h5>Please! Resend OTP</h5>
                </div>
            )
                
            }

            <div>
                <label className='text-white'>OTP</label>
                <input type='text' className='form-control'
                    value={enteredOTP}
                    onChange={(event) => setEnteredOTP(event.target.value)}
                    maxLength={4}
                />
            </div>
            <div className='mt-4 d-flex justify-content-between'>
                {countDown === 0 && 
                    <button className="btn" style={{backgroundColor:'#FE4C00', color:'white'}} onClick={gettingOTP}>
                        {resendingOTP ? (
                            <div class="spinner-border text-success" role="status" style={{width:20,height:20}}>
                                <span class="visually-hidden">Loading...</span>
                            </div>
                        ):"Resend"

                        }
                    </button>

                }
                <button className="btn" style={{backgroundColor:'#FE4C00', color:'white'}} onClick={registerNow}>{registering?<Sentry/>:"Continue"}</button>
            </div>
        </div>

            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
            />

    </div>
  )
}
