import { useLoader } from "@react-three/fiber";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";

function useModelLoader(path) {
    return useLoader(GLTFLoader,path);
}

function ModelLoader() {
    const gltf = useModelLoader('./assets/uploads/girl1.glb');
    console.log(gltf)
    return gltf
}

export default ModelLoader;