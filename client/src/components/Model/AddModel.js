import React, { useEffect, useState } from 'react';
import SelectMultipleOption from '../select/SelectMultipleOption';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {Sentry} from 'react-activity';
import 'react-activity/dist/library.css';

function AddModel(props) {

    console.log("user id",props.id)

    const [modelFile,setModelFile] = useState();
    const [name,setName] = useState();
    const [fileSize, setFileSize] = useState();
    const [selectedEngine,setSelectedEngine] = useState([]);
    const [overview, setOverview] = useState();
    const [typeOfModel, setTypeOfModel] = useState("3D");

    const [uploading,setUploading] = useState(false);

    const handleFileEvent = (event) => {
        event.preventDefault();
        const uploadedFile = event.target.files[0];
        console.log("UPloaded file ",uploadedFile);
        setFileSize(uploadedFile.size);
        setModelFile(uploadedFile);
    }

    function handlerSelected(selectedOption){
        setSelectedEngine(selectedOption);
    }


    const storeModel = async (event) => {
        event.preventDefault();
        setUploading(true)

        if (!modelFile || selectedEngine.length === 0 || !overview || !name ) {
            toast.error("Insufficient metadata of your file", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "colored",
                });
                setUploading(false)
        } else {
            console.log("name",modelFile.name);
            console.log("size ",modelFile.size)
            const reader = new FileReader();
            reader.readAsDataURL(modelFile);
            reader.onload = async () => {
                const modelData = reader.result.split(',')[1];
                const data = {name:name,size:fileSize,file:modelData,engine:selectedEngine,overview:overview,modelType:typeOfModel,userId:props.id}

                const response = await fetch("http://localhost:5000/uploadModel",{
                    method:"POST",
                    headers:{"Content-Type":"application/json"},
                    body:JSON.stringify(data)
                })
                .then(async res => {
                    if (res.ok) {
                        toast.success(await res.json(), {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                            theme: "colored",
                            });
                        setModelFile('');
                        setName('')
                        setFileSize('')
                        setTypeOfModel('3D')
                        setOverview('');
                        props.verifyUpdate(!props.dataUpdated)
                        setUploading(false)
                    } else {
                        toast.error(await res.json(), {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                            theme: "colored",
                        });
                        setUploading(false)
                    }
                })
            }
        }
    }

    console.log(modelFile);
    console.log(fileSize)
    console.log(name)
    console.log(selectedEngine);
    console.log(overview);
    console.log(typeOfModel);

    return(
        <div>
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
            />

            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
            />
            <form onSubmit={storeModel} encType='multipart/form-data'>
                <input className='form-control mb-3' type="file" accept='.glb, image/png' name = "file" onChange={handleFileEvent}/>

                <label>File name*</label>
                <input className='form-control mb-3' placeholder='filename' type="text"  name = "filename" value={name} onChange={(event) => setName(event.target.value)}/>
                <label>Type of asset*</label>
                <select className="form-select mb-3" aria-label="Default select example" value={typeOfModel} onChange={(e) => {
                    setTypeOfModel(e.target.value);
                }}>
                    <option defaultValue value="3D">3D</option>
                    <option value="2D">2D</option>
                </select>
                <label>Description*</label>
                <textarea class="form-control mb-3" rows="3"
                    value={overview} onChange={(event) => {
                        setOverview(event.target.value);
                    }}
                ></textarea>
                <label>Select compatible engine*</label>
                <SelectMultipleOption yourSelection = {selectedEngine} handleSelection = {handlerSelected}/>
                <button type="button ms-auto" class="btn text-white" style={{backgroundColor:"#FE4C00", marginTop:30, paddingRight:20,paddingLeft:20}}>{uploading?<Sentry/>:"Upload"}</button>
            </form>
        </div>
    )
}

export default AddModel;