import * as THREE from 'three';
import { OrbitControls, Stats } from '@react-three/drei';
import { Canvas } from '@react-three/fiber';
import { Environment } from '@react-three/drei';

export default function ModelComponent(props) {


  return(
    <Canvas camera={{position: [0,0,1]}} shadows style={{}}>
      <Environment preset='forest' background blur={0.5}/>
      <primitive
        object = {props.file}
        position = {[0,-0.6,0]}
        children-0-castShadow
      />
      <OrbitControls target = {[0,0,0]} autoRotate = {props.type === "large"?true:false}/>
    </Canvas>
  )
}