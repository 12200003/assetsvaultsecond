const express = require('express');
const app = express();
const pool = require('./db');
const cors = require('cors');
const nodemailer = require("nodemailer");
const bcrypt = require("bcrypt");
const validInfo = require("./middleware/ValidInfo");

app.use(cors());
app.use(express.json({limit:'2000mb'}));
app.use(express.urlencoded({limit:'2000mb', extended:true}))

app.get('/:id', async (req,res) => {
    const id = req.params.id;
    console.log("id ",id)
    await pool.query("SELECT overview,created_at FROM asset WHERE asset_id = $1",[id],(error,results) => {
        if (error) {
            console.error(error);
            res.status(500).send("Error retrieving file");
        } else {
            const fileData = results.rows[0]
            console.log("file data", fileData);
            res.set('Content-Type', 'application/json');
            res.send(fileData);
        }
    })
});

app.get("/", async (req,res) => {
    await pool.query("SELECT * FROM asset", (error,results) => {
        if (error) {
            console.error(error);
            res.status(500).send("Error retrieving file")
        } else {
            const fileData = results.rows;
            console.log(fileData)
            res.set('Content-Type', 'application/json');
            res.send(fileData);
        }
    })
})

//User authentication and authorization
app.use("/auth",require('./routes/jwtAuth'))

app.use("/fdback",require("./routes/feedback"))

app.use("/rating",require("./routes/rating"))

app.use("/admin",require('./routes/admin'));

app.post("/uploadProfile",async (req,res) => {
    try {
        const {userName,email,image, userId} = req.body;
        
        const q = "UPDATE users SET user_name = $1, user_email = $2,profile = decode($3,'base64') WHERE user_id = $4";
        const values = [userName,email,image,userId];

        await pool.query(q,values,(error,results) => {
            if (error) {
                res.status(400).json({error:error.message});
            } else {
                res.status(200).json("Profile Updated Successfully!")
            }
        })

    } catch (error) {
        console.error("from Server Error");
    }
})

app.post("/uploadModel", async (req,res) => {
    try {
        const {name,size,file,overview,engine,modelType,userId} = req.body;

        console.log(engine);

        await pool.query("CREATE TABLE asset (asset_id SERIAL PRIMARY KEY,name VARCHAR(255) NOT NULL,filesize VARCHAR(255) NOT NULL,engine TEXT[],overview VARCHAR(1500) NOT NULL,file VARCHAR(255) NOT NULL, created_at TIMESTAMP NOT NULL DEFAULT NOW(), modelType VARCHAR(255) NOT NULL, user_id uuid)")
        .then(()=>{
            console.log("CREATED SUCCESSFULLY!");
        })
        .catch(error => {
            console.log(error.message);
        })

        const engineData = [] 

        for (let i = 0; i < engine.length; i++) {
            engineData.push(engine[i].value);
        }

        console.log(engineData)

        const response = await pool.query(
            "INSERT INTO asset (name,filesize,engine,overview,file, modeltype, user_id) VALUES ($1,$2,$3,$4,decode($5,'base64'),$6,$7)",
            [name,size,engineData,overview,file,modelType,userId]
        )
        .then(()=>{
            res.status(200).json("Successfully Uploaded!")
        })
        .catch(error => {
            console.log(error.message);
            res.status(401).json("Faild to Upload!")
        })
    } catch (error) {
        console.log(error.message);
        res.status(500).json("Server Error")
    }
})

function generatePassword(length) {
    const chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789#$%*&!';
    let password = '';
    for (let i = 0; i < length; i++) {
      password += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return password;
}

const generateOTP = () => {
    const digits = "0123456789";
    let otp = '';

    for (let i = 0; i < 4; i++) {
        otp += digits[Math.floor(Math.random() * 10)];
    }

    return otp;
}

app.post("/validateUser", validInfo, async (req,res) => {
    try {
        const {name, email, password,confirm} = req.body;

        //check if the user exist 

        const user = await pool.query("SELECT * FROM users WHERE user_email = $1", [
            email
        ])
        
        if (user.rows.length !== 0) {
            return res.status(401).json("User already exists.")
        }
        
        return res.json(true)
    } catch (error) {
        return res.json(false);
    }
})

app.post("/sendOTP",validInfo, async (req,res) => {
    try {
        const {email} = req.body;
        const otp = generateOTP();
        console.log(otp);

        const transporter = nodemailer.createTransport({
            host: 'smtp.zoho.com',
            port: 465,
            secure: true,
            auth: {
              user: '12200003.gcit@zohomail.com',
              pass: 'dalboys66'
            }
        });

        const mailOptions = {
            from: '12200003.gcit@zohomail.com',
            to: email,
            subject: "assetsVault",
            text: `Your OTP is ${otp}`
        };

        transporter.sendMail(mailOptions, async (error, info) => {
            if (error) {
                console.log(error.message);
              return res.json(error.message);
            } else {
             return res.json({otp:otp, success:true})
            }
        });

    } catch (error) {
        console.log(error.message);
        return res.json({error:false});
    }
})

app.post("/sendPassword", async (req,res) => {
    const { email } = req.body;
    const password = generatePassword(10);
    console.log(password)

    const saltRound = 10
    const salt = await bcrypt.genSalt(saltRound);
    const bcryptPassword = await bcrypt.hash(password,salt);
    console.log(bcryptPassword)

    try {

        const user = await pool.query("SELECT * FROM users WHERE user_email = $1",[email]);

        if (user.rows.length === 0) {
            res.status(500).json("User not found")
        }

        const transporter = nodemailer.createTransport({
            host: 'smtp.zoho.com',
            port: 465,
            secure: true,
            auth: {
              user: '12200003.gcit@zohomail.com',
              pass: 'dalboys66'
            }
        });

        const mailOptions = {
            from: '12200003.gcit@zohomail.com',
            to: email,
            subject: "assetsVault",
            text: `Your password is ${password}`
        };
          
        transporter.sendMail(mailOptions, async (error, info) => {
            if (error) {
              console.log(error);
            } else {
              console.log('Email sent: ' + info.response);
              const updatingPassword = await pool.query("UPDATE users SET user_password = $1 WHERE user_email = $2 RETURNING *",[bcryptPassword,email])

              if (updatingPassword.rows.length !== 0) {
                res.status(200).json("true")
              } else {
                res.status(401).json("false")
              }
            }
        });

    } catch (error) {
        console.log(error.message);
    }
})



const serve = app.listen(5000,()=>{
    console.log("Server running on port 5000");
})


app.set('trust proxy', true)