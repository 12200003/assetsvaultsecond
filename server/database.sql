CREATE DATABASE assetsvault;

--set extension
CREATE TABLE users(
    user_id uuid PRIMARY KEY DEFAULT uuid_generated_v4(),
    user_name VARCHAR(255) NOT NULL,
    user_email VARCHAR(255) NOT NULL,
    user_password VARCHAR(255) NOT NULL
)

--insert fake users
INSERT INTO users (user_name,user_email,user_password) 
VALUES ("Dorji Lethro","12200003.gcit@rub.edu.bt","dalboys66")

--feedback

CREATE TABLE modelFeedback(
  feed_id UUID PRIMARY KEY,
  message VARCHAR(255),
  user_id UUID REFERENCES users(user_id),
  asset_id INTEGER REFERENCES asset(asset_id),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE rating (rate_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),rate INT NOT NULL,user_id UUID NOT NULL,asset_id INT NOT NULL,FOREIGN KEY (user_id) REFERENCES users(user_id),FOREIGN KEY (asset_id) REFERENCES asset(asset_id));

INSERT INTO rates (rate_id, rate, user_id, asset_id) VALUES ('<rate_id>', <rate>, '<user_id>', <asset_id>) ON CONFLICT (rate_id) DO UPDATE SET rate = EXCLUDED.rate, user_id = EXCLUDED.user_id, asset_id = EXCLUDED.asset_id;

CREATE TABLE assetReport (report_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),report VARCHAR(255) NOT NULL,user_id UUID NOT NULL,asset_id INT NOT NULL,FOREIGN KEY (user_id) REFERENCES users(user_id),FOREIGN KEY (asset_id) REFERENCES asset(asset_id));