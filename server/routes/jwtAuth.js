const router = require('express').Router();
const pool = require("../db")
const bcrypt = require("bcrypt");
const jwtGenerator = require('../utils/jwtGenerator');
const validInfo = require("../middleware/ValidInfo");
const authorization = require("../middleware/authorization");

//registering
router.post("/register",validInfo, async (req,res) => {
    try {
        //destructure the req.body 

        const {name, email, password,confirm} = req.body;

        //check if the user exist 

        const user = await pool.query("SELECT * FROM users WHERE user_email = $1", [
            email
        ])
        
        if (user.rows.length !== 0) {
            return res.status(401).json("User already exists.")
        }

        //bcrypt the user password 

        const saltRound = 10
        const salt = await bcrypt.genSalt(saltRound);
        const bcryptPassword = await bcrypt.hash(password,salt);

        //store the user

        const newUser = await pool.query("INSERT INTO users (user_name,user_email,user_password) VALUES ($1,$2,$3) RETURNING *",[
            name,email,bcryptPassword
        ])

        //generating out jwt token

        const token = jwtGenerator(newUser.rows[0].user_id);

        res.json({ token });
        
    } catch (error) {
        console.log("coming from register")
        console.error(error.message);
        res.status(500).send("Server Error")
    }
})

// login 

router.post("/login", validInfo, async (req,res) => {
    try {
        // destructure the req.body 

        const {email,password} = req.body;

        // check if user doesn't exists

        const user = await pool.query("SELECT * FROM users WHERE user_email = $1",[
            email
        ]);
        if (user.rows.length === 0) {
            return res.status(401).json("Password or Email is invalid!");
        }

        // check if incoming password is as same as db password 

        const validPassword = await bcrypt.compare(password,user.rows[0].user_password)

        if (!validPassword) {
            return res.status(401).json("Password or Email is Invalid");
        }

        // give them the jwt token

        const token = jwtGenerator(user.rows[0].user_id);
        const tempUserId = user.rows[0].user_id;
        res.json({token,userId:tempUserId});
        
    } catch (error) {
        console.error(error.message);
        res.status(500).send("Server Error")
    }
})

router.get("/is-verified",authorization, async (req,res) => {
    try {
        res.json(true)
    } catch (error) {
        console.error(error.message);
        res.status(500).send("Server Error")
    }
})

router.get('/user', authorization, async (req,res) => {
    console.log("in user getting process");
    try {
        const user = await pool.query("SELECT user_id,user_name,user_email,encode(profile,'base64') FROM users WHERE user_id = $1",[req.user]);
        res.json(user.rows[0]);
    } catch (error) {
        console.error(error.message);
        res.status(500).json("Server Error");
    }
});

router.get('/model', async (req,res) => {
    console.log("in glb getting process");
    try {
        const user = await pool.query("SELECT asset_id,name,filesize,engine,overview,encode(file,'base64'),modeltype,user_id FROM asset WHERE modeltype = $1 ORDER BY created_at DESC",["3D"]);
        res.json(user.rows);
    } catch (error) {
        console.error(error.message);
        res.status(500).json("Server Error");
    }
});

router.get('/searchModel', async (req,res) => {
    console.log("in glb getting process");
    try {
        const user = await pool.query("SELECT asset_id,name,engine FROM asset WHERE modeltype = $1",["3D"]);
        if (user.rows.length !== 0) {
            console.log("searching model ", user.rows);
            res.json(user.rows);
        }
    } catch (error) {
        console.error(error.message);
        res.status(500).json("Server Error");
    }
});

router.get('/searchImage', async (req,res) => {
    console.log("in glb getting process");
    try {
        const user = await pool.query("SELECT asset_id,name,engine FROM asset WHERE modeltype = $1",["2D"]);
        if (user.rows.length !== 0) {
            console.log("searching model ", user.rows);
            res.json(user.rows);
        } else {
            res.json([]);
        }
    } catch (error) {
        console.error(error.message);
        res.status(500).json("Server Error");
    }
});

router.get("/getTwoDimension", async (req,res) => {
    try {
        const twoDimension = await pool.query("SELECT asset_id,name,filesize,engine,overview,encode(file,'base64'),modeltype,user_id FROM asset WHERE modeltype = $1 ORDER BY created_at DESC",["2D"]);
        if (twoDimension.rows.length !== 0) {
            res.status(200).json({"data":twoDimension.rows})
        } else {
            res.status(401).json({"empty":"Noting to show."})
        }
    } catch (error) {
        console.error(error.message);
    }
})

router.get('/model/:id', async (req,res) => {
    console.log("in glb getting process");
    const {id}  = req.params;
    console.log(id);
    try {
        const user = await pool.query("SELECT asset_id,name,filesize,engine,overview,encode(file,'base64'),modeltype,user_id FROM asset WHERE asset_id = $1",[id]);
        console.log("passing ",user.rows[0])
        res.json(user.rows[0]);
    } catch (error) {
        console.error(error.message);
        res.status(500).json("Server Error");
    }
});

router.get('/smodel/:id', async (req,res) => {
    console.log("in glb getting process");
    const {id}  = req.params;
    console.log(id);
    try {
        const user = await pool.query("SELECT asset_id,name,filesize,user_id FROM asset WHERE asset_id = $1",[id]);
        console.log("passing ",user.rows[0])
        res.json(user.rows[0]);
    } catch (error) {
        console.error(error.message);
        res.status(500).json("Server Error");
    }
});

router.get('/singleModel/:userId', async (req,res) => {
    const {userId}  = req.params;
    console.log(typeof userId)
    try {
        const user = await pool.query("SELECT asset_id,name,filesize,engine,overview,encode(file,'base64'),modeltype FROM asset WHERE user_id = $1 AND modeltype = $2",[userId,"3D"]);
        res.json(user.rows);
    } catch (error) {
        console.error(error.message);
        res.status(500).json("Server Error");
    }
});

router.get('/singleModel/twod/:userId', async (req,res) => {
    console.log("in getting process image");
    const {userId}  = req.params;
    console.log("userId is ",userId)

    try {
        console.log("The error is getting from there")
        const user = await pool.query("SELECT asset_id,name,filesize,engine,overview,encode(file,'base64'),modeltype FROM asset WHERE user_id = $1 AND modeltype = $2",[userId,"2D"]);
        console.log("passing ",user.rows)
        if (user.rows === 0) {
            res.status(500).json({"error":"Empty"});
        } else {
            res.status(200).json({"success":user.rows})
        }
    } catch (error) {
        console.error("this error ",error.message);
        res.status(500).json("Server Error");
    }
});

router.post('/changePassword/:userId', async (req,res) => {
    const {userId} = req.params;
    try {
        const {newPassword} = req.body;

        const saltRound = 10
        const salt = await bcrypt.genSalt(saltRound);
        const bcryptPassword = await bcrypt.hash(newPassword,salt);

        const user = await pool.query("UPDATE users SET user_password = $1 WHERE user_id = $2 RETURNING *",[bcryptPassword,userId]);

        if (user.rows.length !== 0) {
            res.status(200).json("Password Changed Successfully!")
        } else {
            res.json("Something went wrong!")
        }

        console.log("server responding", userId)
    } catch (error) {
        console.log(error.message);
        res.json("Server Error");
    }
})

module.exports = router;