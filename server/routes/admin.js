const router = require('express').Router();
const pool = require('../db');

router.get("/users",async(req,res)=>{
    try {
        const allUsers=await pool.query("SELECT user_id,user_name,user_email,encode(profile,'base64') FROM users");
        res.json(allUsers.rows);
    } catch (err) {
        console.error(err.message)
    }
})

//---------------------------------------------------------------------//
// deleting a users

router.delete("/users/:id",async(req,res)=>{
    const userId = req.params.id;
    console.log(userId)

  try {
    const client = await pool.connect();

    try {
      await client.query('BEGIN');

      // Delete records from the rating_table related to the user_id
      await client.query('DELETE FROM rating WHERE user_id = $1', [userId]);

      // Delete records from the feedback_table related to the user_id
      await client.query('DELETE FROM modelfeedback WHERE user_id = $1', [userId]);

      // Deleting assets repot
      await client.query("DELETE FROM assetreport WHERE user_id =$1",[userId])

      // Delete records from the asset_table related to the user_id
      await client.query('DELETE FROM asset WHERE user_id = $1', [userId]);

      // Delete the user from the user_table
      await client.query('DELETE FROM users WHERE user_id = $1', [userId]);

      await client.query('COMMIT');
      res.sendStatus(200);
    } catch (error) {
      await client.query('ROLLBACK');
      throw error;
    } finally {
      client.release();
    }
  } catch (error) {
    console.error('Error occurred during user deletion:', error);
    res.sendStatus(500);
  }
});


//-------------------------------------------------------//
// getting All assets
router.get("/assets", async (req, res) => {
  try {
    const allAssets = await pool.query(`
      SELECT 
        asset.asset_id,
        asset.name,
        asset.filesize,
        asset.engine,
        asset.overview,
        asset.modeltype,
        encode(asset.file, 'base64') AS file,
        ROUND(AVG(rating.rate), 1) AS average_rating
      FROM 
        asset
      LEFT JOIN 
        rating ON asset.asset_id = rating.asset_id
      GROUP BY
        asset.asset_id
    `);

    res.json(allAssets.rows);
  } catch (err) {
    console.error(err.message);
  }
});


//--------------------------------------------------------------//
//deleting a assets
router.delete('/assets/:id', async (req, res) => {
  const assetId = req.params.id;
  console.log("assest id: ", assetId); // Verify if the assetId is correctly retrieved from the URL

  try {
    const client = await pool.connect();

    try {
      await client.query('BEGIN');

      // Delete records from the feedback_table related to the asset_id
      await client.query('DELETE FROM modelfeedback WHERE asset_id = $1', [assetId]);

      // Delete records from the rating_table related to the asset_id
      await client.query('DELETE FROM rating WHERE asset_id = $1', [assetId]);
      
      // Deleting asset report
      await client.query("DELETE FROM assetreport WHERE asset_id =$1",[assetId])

      // Delete the asset from the asset_table
      await client.query('DELETE FROM asset WHERE asset_id = $1', [assetId]);

      await client.query('COMMIT');
      res.sendStatus(200);
    } catch (error) {
      await client.query('ROLLBACK');
      throw error;
    } finally {
      client.release();
    }
  } catch (error) {
    console.error('Error occurred during asset deletion:', error);
    res.sendStatus(500);
  }
});

//-------------------------------------------------------------//
// getting total users
router.get('/totalUsers', async(req, res) => {
    await pool.query('SELECT COUNT(*) FROM users', (error, result) => {
      if (error) {
        console.error('Error executing query', error);
        res.status(500).json({ error: 'Internal server error' });
        return;
      }
  
      const totalUsers = result.rows[0].count;
      res.json({ totalUsers });
    });
});

//-------------------------------------------------------------//
// getting total Assets
router.get('/totalAssets', async(req, res) => {
    await pool.query('SELECT COUNT(*) FROM asset', (error, result) => {
      if (error) {
        console.error('Error executing query', error);
        res.status(500).json({ error: 'Internal server error' });
        return;
      }
  
      const totalAssets = result.rows[0].count;
      res.json({ totalAssets });
    });
});


module.exports = router;